package com.gitlab.mcyttwthp.eepresent

import com.gitlab.mcyttwthp.eepresent.commands.PresentCommand
import com.gitlab.mcyttwthp.eepresent.events.PresentRightClickEvent
import com.gitlab.mcyttwthp.eepresent.utils.Presents
import org.bukkit.Location
import org.bukkit.Particle
import org.bukkit.plugin.java.JavaPlugin
import org.bukkit.scheduler.BukkitTask

class Main : JavaPlugin() {
    override fun onEnable() {
        plugin = this

        if (!this.dataFolder.exists())
            this.dataFolder.mkdir()

        this.getCommand("present")?.setExecutor(PresentCommand)
        this.getCommand("present")?.tabCompleter = PresentCommand

        this.server.pluginManager.registerEvents(PresentRightClickEvent, this)

        particleTask = this.server.scheduler.runTaskTimer(this, Runnable {
            Presents.presents.forEach {
                this.server.onlinePlayers.forEach {itt ->
                    if (!it.value.acquired.contains(itt.uniqueId)) {
                        val loc = Location(it.value.world, it.value.position.blockX + 0.5, it.value.position.blockY + 0.6, it.value.position.blockZ + 0.5)
                        itt.spawnParticle(Particle.VILLAGER_HAPPY, loc, 10)
                    }
                }
            }
        }, 50, 45)
    }

    override fun onDisable() {
        this.server.scheduler.cancelTask(particleTask.taskId)
        Presents.save()
    }

    companion object {
        lateinit var plugin: JavaPlugin
        private lateinit var particleTask: BukkitTask
    }
}