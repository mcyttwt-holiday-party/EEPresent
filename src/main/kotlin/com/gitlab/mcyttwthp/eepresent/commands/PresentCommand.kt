package com.gitlab.mcyttwthp.eepresent.commands

import com.gitlab.mcyttwthp.eepresent.Main
import com.gitlab.mcyttwthp.eepresent.utils.Colors
import com.gitlab.mcyttwthp.eepresent.utils.Present
import com.gitlab.mcyttwthp.eepresent.utils.PresentTypes
import com.gitlab.mcyttwthp.eepresent.utils.Presents
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player
import kotlin.math.min

object PresentCommand : CommandExecutor, TabCompleter {
    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        return when (args.size) {
            1 -> {
                val list = mutableListOf("stats", "leaderboard")
                if (sender.hasPermission("eepresent.admin"))
                    list.add("admin")

                list
            }

            2 -> {
                if (args[0].toLowerCase() == "stats") {
                    val list = Main.plugin.server.onlinePlayers.map { it.name }.toMutableList()
                    list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                    list
                } else if (args[0].toLowerCase() == "admin" && sender.hasPermission("eepresent.admin")) {
                    mutableListOf("set", "movehere", "change", "reset-acquired", "list", "remove", "del", "delete", "rotate", "id")
                } else {
                    mutableListOf("")
                }
            }

            3 -> {
                if (args[0] != "admin" || !sender.hasPermission("eepresent.admin"))
                    return mutableListOf("")

                return when (args[1].toLowerCase()) {
                    "set" -> {
                        PresentTypes.values().map { it.name.toLowerCase() }.toMutableList()
                    }

                    "change",
                    "movehere", "remove", "del", "delete", "rotate" -> {
                        Presents.presents.keys.toMutableList()
                    }

                    "reset-acquired" -> {
                        val list = mutableListOf("-a")
                        list.addAll(Main.plugin.server.onlinePlayers.map { it.name })
                        list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                        list
                    }

                    else -> mutableListOf("")
                }
            }

            4 -> {
                if (args[0] != "admin" || !sender.hasPermission("eepresent.admin"))
                    return mutableListOf("")

                return when (args[1].toLowerCase()) {
                    "change" -> PresentTypes.values().map { it.name.toLowerCase() }.toMutableList()
                    "reset-acquired" -> {
                        val list = mutableListOf("-a")
                        list.addAll(Main.plugin.server.onlinePlayers.map { it.name })
                        list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                        list
                    }

                    else -> mutableListOf("")
                }
            }

            else -> {
                if (args[1].toLowerCase() == "reset-acquired" && args[0].toLowerCase() == "admin" && sender.hasPermission("eepresent.admin")) {
                    val list = mutableListOf("-a")
                    list.addAll(Main.plugin.server.onlinePlayers.map { it.name })
                    list.addAll(Main.plugin.server.offlinePlayers.map {it.name ?: ""})

                    list
                } else
                    mutableListOf("")
            }
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (args.isEmpty()) return false

        when (args[0].toLowerCase()) {
            "stats" -> {
                if (args.size == 2 && !Main.plugin.server.onlinePlayers.any { it.name == args[1] } && !Main.plugin.server.offlinePlayers.any { it.name == args[1] }) {
                    sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player does not exist!")
                    return true
                }

                val player = if (args.size == 2)
                        try {
                            Main.plugin.server.onlinePlayers.first { it.name.toLowerCase() == args[1].toLowerCase() }
                        } catch (_: Exception) {
                            try {
                                Main.plugin.server.offlinePlayers.first { it.name?.toLowerCase() == args[1].toLowerCase() } ?: return false
                            } catch (_: Exception) {
                                sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}That player does not exist!")

                                return true
                            }
                        }
                    else {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You are not a player!")
                            return true
                        }

                        sender
                    }

                val presentStats = collectStats(player)

                if (presentStats.isNullOrEmpty()) {
                    sender.sendMessage("${Colors.PREFIX}Player ${ChatColor.YELLOW}${player.name}${ChatColor.RESET} doesn't seem to have collected any presents. Please encourage them to collect some!")
                    return true
                }

                sender.sendMessage("${Colors.PREFIX}Presents collected by ${ChatColor.YELLOW}${player.name}${ChatColor.RESET}:")
                presentStats.forEach {
                    sender.sendMessage("    ${ChatColor.GREEN}${presentStats.indexOf(it) + 1}. ${colorFromType(it.type)}${it.name} ${if (player == (sender as Player)) "${ChatColor.GOLD}(${it.position.x} ${it.position.y} ${it.position.z})" else ""}")
                }

                sender.sendMessage("${ChatColor.GREEN}Total collected presents by ${ChatColor.YELLOW}${player.name}${ChatColor.GREEN}: ${ChatColor.AQUA}${presentStats.size} ${ChatColor.YELLOW}/ ${ChatColor.DARK_AQUA}${Presents.presents.size}")

                return true
            }

            "leaderboard" -> {
                val players = Main.plugin.server.offlinePlayers.toMutableList()

                val statsList = players.map { Pair(it, collectStats(it).size) }.sortedByDescending { it.second }.subList(0, min(9.0, players.size.toDouble()).toInt())

                //println(statsList.joinToString(", ") { it.first.name })

                sender.sendMessage("${Colors.PREFIX}Collected presents leaderboard :")
                statsList.forEach {
                    sender.sendMessage("    ${ChatColor.GREEN}${statsList.indexOf(it) + 1}. ${if (it.first.isOnline) ChatColor.GREEN else ChatColor.RED}${it.first.name} ${ChatColor.DARK_GREEN} - ${ChatColor.YELLOW}${it.second}")
                }

                return true
            }

            "admin" -> {
                if (args.size == 1) return false

                when (args[1].toLowerCase()) {
                    "set" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }

                        try {
                            val type = if (args.size == 2) PresentTypes.values()
                                .random() else PresentTypes.valueOf(args[2].toUpperCase())

                            val id = randomString(6)

                            val present = Present(
                                id,
                                name = if (args.size == 3) "PresentID$id" else args.slice(3 until args.size).joinToString(" "),
                                type = type,
                                position = sender.location,
                                world = sender.world,
                                acquired = mutableListOf()
                            )

                            Presents.presents[id] = present
                            Presents.spawnPresent(id)

                            Presents.save()

                            sender.sendMessage("${Colors.PREFIX}Present created with ID ${ChatColor.YELLOW}$id${ChatColor.RESET}!")
                        } catch (e: IllegalArgumentException) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Invalid present type!")
                        } catch (e: Exception) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst setting the present: ${ChatColor.DARK_RED}${e.message}")
                            e.printStackTrace()
                        }

                        return true
                    }

                    "movehere" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }
                        
                        if (args.size == 2)
                            return false

                        val present = Presents.presents[args[2]]

                        if (present == null) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The specified present doesn't exist!")
                            return true
                        }

                        present.position.block.type = Material.AIR
                        
                        present.position.set(sender.location.x, sender.location.y, sender.location.z)

                        Presents.spawnPresent(present.id)

                        sender.sendMessage("${Colors.PREFIX}Position for present modified!")

                        return true
                    }

                    "change" -> {
                        if (args.size == 2)
                            return false

                        try {
                            val present = Presents.presents[args[2]]

                            if (present == null) {
                                sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The specified present doesn't exist!")
                                return true
                            }

                            present.type =
                                if (args.size == 4) PresentTypes.valueOf(args[3].toUpperCase()) else PresentTypes.values()
                                    .random()

                            sender.sendMessage("${Colors.PREFIX}Changed the present type to ${colorFromType(present.type)}${present.type.name}${ChatColor.RESET}!")

                            Presents.spawnPresent(present.id)

                            return true
                        } catch (e: IllegalArgumentException) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Invalid present type!")

                            return true
                        } catch (e: Exception) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}An exception occurred whilst changing the present type: ${ChatColor.DARK_RED}${e.message}")
                            return true
                        }
                    }

                    "reset-acquired" -> {
                        if (!sender.hasPermission("eepresent.admin.reset")) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You don't have permission to reset acquired status!")
                            return true
                        }

                        if (args.size == 2) {
                            return false
                        }

                        if (args[2].toLowerCase() == "-a") {
                            Presents.presents.forEach {
                                it.value.acquired.clear()
                            }

                            Presents.save()
                            sender.sendMessage("${Colors.PREFIX}Reset all acquired status.")

                            return true
                        }

                        args.toList().subList(2, args.size).forEach {
                            val player = Main.plugin.server.getPlayer(it) ?: Main.plugin.server.getOfflinePlayerIfCached(it)?.player

                            if (player == null) {
                                sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}Player ${ChatColor.YELLOW}$it ${ChatColor.RED}doesn't exist!")
                                return true
                            }

                            Presents.presents.forEach { itt ->
                                itt.value.acquired.remove(player.uniqueId)
                            }

                            Presents.save()

                            sender.sendMessage("${Colors.PREFIX}Reset acquired status for ${ChatColor.YELLOW}$it${ChatColor.RESET}!")
                        }

                        return true
                    }

                    "list" -> {
                        sender.sendMessage("${Colors.PREFIX}Listing all presents...")

                        Presents.presents.forEach {
                            sender.sendMessage("    - ${colorFromType(it.value.type)}${it.value.name} ${ChatColor.YELLOW}(ID : ${it.key}) ${ChatColor.GOLD}(${it.value.position.blockX} ${it.value.position.blockY} ${it.value.position.blockZ})")
                        }

                        return true
                    }

                    "remove", "del", "delete" -> {
                        if (args.size == 2) return false

                        val id = args[2]

                        val present = Presents.presents[id]

                        if (present == null) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}The present doesn't exist!")
                            return true
                        }

                        sender.sendMessage("${Colors.PREFIX}Successfully removed ${colorFromType(present.type)}${present.name} ${ChatColor.YELLOW}(ID : $id) ${ChatColor.GOLD}(${present.position.blockX} ${present.position.blockY} ${present.position.blockZ})")

                        present.position.block.type = Material.AIR
                        Presents.presents.remove(id, present)
                        Presents.save()

                        return true
                    }

                    "rotate" ->  {
                        sender.sendMessage("${ChatColor.GREEN}Tip${ChatColor.GOLD}: ${ChatColor.AQUA}Use the ${ChatColor.BLUE}Debug Stick ${ChatColor.AQUA}to rotate the present. It's far faster.")

                        return true
                    }

                    "id" -> {
                        if (sender !is Player) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not a player!")
                            return true
                        }

                        val targetedBlock = sender.getTargetBlock(10)

                        if (targetedBlock == null || !Presents.presents.any { it.value.position.block == targetedBlock }) {
                            sender.sendMessage("${Colors.PREFIX}${ChatColor.RED}You're not facing a present! To use this command, you must be looking at a present that is within 10 blocks away.")
                            return true
                        }

                        val present = Presents.presents.filter { it.value.position.block == targetedBlock }.values.first()

                        sender.sendMessage("${Colors.PREFIX}The present you're looking at is ${colorFromType(present.type)}${present.name} ${ChatColor.GOLD}(ID : ${present.id})${ChatColor.RESET}.")

                        return true
                    }

                    else -> return false
                }
            }
        }

        return false
    }

    private fun collectStats(player: OfflinePlayer): List<Present> {
        val list = Presents.presents.filter { it.value.acquired.contains(player.uniqueId) }

        return list.values.toList()
    }

    fun colorFromType(type: PresentTypes): ChatColor {
        return when (type) {
            PresentTypes.GREEN,
            PresentTypes.GREEN2 -> ChatColor.GREEN

            PresentTypes.GOLD -> ChatColor.GOLD

            PresentTypes.CYAN -> ChatColor.DARK_AQUA

            PresentTypes.BLUE -> ChatColor.BLUE
            PresentTypes.RED -> ChatColor.RED
        }
    }

    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    private fun randomString(length: Int): String {
        return (1..length).map {
            kotlin.random.Random.nextInt(0, charPool.size)
        }
            .map(charPool::get)
            .joinToString("")
    }
}

/*
Admin commands:

set [type] [name]
movehere <id>
change <id> [type]
reset-acquired <name / -a>
 */