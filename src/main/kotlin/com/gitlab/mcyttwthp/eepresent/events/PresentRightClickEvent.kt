package com.gitlab.mcyttwthp.eepresent.events

import com.destroystokyo.paper.event.block.BlockDestroyEvent
import com.gitlab.mcyttwthp.eepresent.commands.PresentCommand
import com.gitlab.mcyttwthp.eepresent.utils.Colors
import com.gitlab.mcyttwthp.eepresent.utils.Presents
import org.bukkit.ChatColor
import org.bukkit.Sound
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.EquipmentSlot

object PresentRightClickEvent : Listener {
    @EventHandler
    fun onPresentRightClick(ev: PlayerInteractEvent) {
        if (ev.action == Action.RIGHT_CLICK_BLOCK && Presents.presents.any { it.value.position.block == ev.clickedBlock } && ev.hand == EquipmentSlot.HAND && !ev.hasItem()) {
            ev.isCancelled = true
            val present = Presents.presents.filter { it.value.position.block == ev.clickedBlock }.values.first()

            if (present.acquired.contains(ev.player.uniqueId)) {
                ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}You have already acquired this present!")
                return
            }

            ev.player.playSound(ev.player.location, Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.0F)
            ev.player.sendMessage("${Colors.PREFIX}You have acquired the \"${PresentCommand.colorFromType(present.type)}${present.name}${ChatColor.RESET}\" present!")

            present.acquired.add(ev.player.uniqueId)

            Presents.presents[present.id] = present

            Presents.save()
        }
    }

    @EventHandler
    fun onPresentBreak(ev: BlockBreakEvent) {
        if (Presents.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true

            ev.player.sendMessage("${Colors.PREFIX}${ChatColor.RED}Don't break the present!")
        }
    }

    @EventHandler
    fun onPresentDestroy(ev: BlockDestroyEvent) {
        if (Presents.presents.any { it.value.position.block == ev.block }) {
            ev.isCancelled = true
        }
    }
}