package com.gitlab.mcyttwthp.eepresent.utils

import org.bukkit.ChatColor

object Colors {
    val PREFIX = "${ChatColor.GOLD}EE${ChatColor.GREEN}${ChatColor.BOLD}Presents ${ChatColor.AQUA}>> ${ChatColor.RESET}"
}