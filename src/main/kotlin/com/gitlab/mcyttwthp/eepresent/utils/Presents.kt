package com.gitlab.mcyttwthp.eepresent.utils

import com.destroystokyo.paper.profile.ProfileProperty
import com.gitlab.mcyttwthp.eepresent.Main
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.Skull
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import java.io.File
import java.net.URI
import java.net.URISyntaxException
import java.util.*


object Presents {
    private val config = if (File(Main.plugin.dataFolder, "presents.yml").exists())
        YamlConfiguration.loadConfiguration(File(Main.plugin.dataFolder, "presents.yml"))
    else
        YamlConfiguration.loadConfiguration("".reader())

    val presents = mutableMapOf<String, Present>()

    init {
        if (config.contains("presents") && config.getMapList("presents").isNotEmpty()) {
            config.getMapList("presents").forEach {
                // If the world doesn't exist, don't bother adding the present.
                val world = Main.plugin.server.getWorld(it["world"] as String) ?: return@forEach
                val positions = it["position"] as List<Int>
                val playersList = mutableListOf<UUID>()
                playersList.addAll(Main.plugin.server.onlinePlayers.toList().filter { itt -> (it["acquired"] as List<String>).contains(itt.uniqueId.toString()) }.map { itt -> itt.uniqueId })
                playersList.addAll(Main.plugin.server.offlinePlayers.mapNotNull { itt -> itt.player }.toMutableList().filter { itt -> (it["acquired"] as List<String>).contains(itt.uniqueId.toString()) }.map { itt -> itt.uniqueId })

                presents[it["id"] as String] = Present(
                    id = it["id"] as String,
                    name = it["name"] as String,
                    type = PresentTypes.fromInt(it["type"] as Int),
                    position = Location(world, positions[0].toDouble(), positions[1].toDouble(), positions[2].toDouble()),
                    world = world,
                    acquired = playersList
                )

                /*if (it["entity"] != null && world.getEntity(UUID.fromString(it["entity"] as String)) != null) {
                    presents[it["id"] as String]!!.entity = world.getEntity(UUID.fromString(it["entity"] as String))!! as ArmorStand
                } else {
                    presents[it["id"] as String]!!.entity = spawnPresent(it["id"] as String)
                }*/
                
                if (world.getBlockAt(positions[0], positions[1], positions[2]).type != Material.PLAYER_HEAD) {
                    spawnPresent(it["id"] as String)
                }
            }
        }
    }

    fun spawnPresent(id: String) {
        val present = presents[id] ?: throw Exception("Present ID doesn't exist!")

        val skin = getSkin(present.type)

        /*val armorStand = present.world.spawn(present.position.subtract(0.0, 0.7, 0.0), ArmorStand::class.java)

        armorStand.isSmall = true
        armorStand.isInvisible = true
        armorStand.isInvulnerable = true
        armorStand.isMarker = true
        armorStand.setGravity(false)
        armorStand.setDisabledSlots(EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.HAND, EquipmentSlot.HEAD, EquipmentSlot.LEGS, EquipmentSlot.OFF_HAND)
        armorStand.setBasePlate(false)
        armorStand.customName = "PresentEE${present.id}"*/

        /*val presentHead = ItemStack(Material.PLAYER_HEAD)
        val meta = presentHead.itemMeta as SkullMeta*/
        val b64 = urlToBase64(skin)!!

        val playerProfile = Bukkit.createProfile(UUID(
            b64.substring(b64.length - 20).hashCode().toLong(),
            b64.substring(b64.length - 10).hashCode().toLong()
        ), "PRESENT_${present.type.name}")
        playerProfile.properties.add(ProfileProperty("textures", b64))
        playerProfile.complete()

        /*meta.playerProfile = playerProfile
        presentHead.itemMeta = meta*/

        val block = present.world.getBlockAt(present.position)
        block.type = Material.PLAYER_HEAD

        val state = block.state as Skull
        state.setPlayerProfile(playerProfile)
        state.update()

        //armorStand.equipment?.helmet = presentHead
    }

    fun save() {
        if (presents.isNotEmpty()) {
            val section = mutableListOf<Any?>()
            presents.forEach {
                val map = mutableMapOf<String, Any>()

                map["id"] = it.key
                map["name"] = it.value.name
                map["type"] = it.value.type.value
                map["position"] = listOf(it.value.position.x, it.value.position.y, it.value.position.z)
                map["world"] = it.value.world.name

                val acquired = mutableListOf<String>()
                it.value.acquired.forEach {itt ->
                    acquired.add(itt.toString())
                }

                map["acquired"] = acquired

                section.add(map)
            }

            config.set("presents", section)
        }

        if (!Main.plugin.dataFolder.exists())
            Main.plugin.dataFolder.mkdir()

        config.save(File(Main.plugin.dataFolder, "presents.yml"))
    }

    private fun getSkin(type: PresentTypes): String {
        return "https://textures.minecraft.net/texture/" + when (type) {
            PresentTypes.BLUE -> "48afb431575cf794f1a8dab3602278380ff9886ae2cd4fe71aba37c1a9453bc9"
            PresentTypes.CYAN -> "df5898ad54e634c59fab5b284d49b3e25d015512caa3ab5620cecf00b84f1345"
            PresentTypes.GREEN -> "301b2cee6b3688f75e02ef4a740ee67ea42ac537d8ca401c200608ae02608b5"
            PresentTypes.GOLD -> "20f2316e455ac3c3fa2e0ed52f305cc4bc363abaff1553bb7170b0dc8368abc1"
            PresentTypes.GREEN2 -> "f73b8769b461e865cb85d0e05a348cc412ec701b8fca99dd5d464c9e27f9b440"
            PresentTypes.RED -> "1489e5a55a1225bbb721c92f45453ac110a262d0ac43223c9783157d8f21d50f"
        }
    }

    private fun urlToBase64(url: String): String? {
        val actualUrl: URI
        try {
            actualUrl = URI(url)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }
        val toEncode = "{\"textures\":{\"SKIN\":{\"url\":\"$actualUrl\"}}}"

        return Base64.getEncoder().encodeToString(toEncode.toByteArray())
    }
}

/*
Present file structure :

presents:
    - id: 4a92b33f
      name: "Void Hollow"
      type: 0
      position:
        - 127
        - 42
        - -446
      world: "minecraft:overworld"
      acquired:
        - 031c8ca803b141a3a745fc0415a67adf # BluSpring_YT
 */